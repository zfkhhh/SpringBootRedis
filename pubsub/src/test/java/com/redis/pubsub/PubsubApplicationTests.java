package com.redis.pubsub;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.RedisTemplate;

import java.util.Date;

@SpringBootTest
class PubsubApplicationTests {

    @Autowired
    private RedisTemplate<String,Object> template;

    @Test
    void contextLoads() {

        for (int i = 0;i < 5;i++){
            try {
                Thread.sleep(2000);
            }
            catch (Exception e){
                e.getMessage();
            }
            template.convertAndSend("Test",String.format("我是消息{%d}号: %tT", i, new Date()));
        }

    }



}
