package com.redis.pubsub.util;

import org.springframework.stereotype.Component;

/**
 * Author : zfk
 * Data : 11:40
 */

@Component
public class RedisReceiver {

    //收到通道的消息后执行的监听方法：要和Config中MessageListenerAdapter对应
    public void receiveMessage(String message){
        System.out.println(message);
    }
}
