package com.lettuce;

import com.lettuce.po.User;
import com.lettuce.service.UserService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class LettuceApplicationTests {

    @Autowired
    private UserService userService;
    @Test
    void contextLoads() {

        String string = userService.getString("1111");

        System.out.println(string);

    }

    @Test
    void test(){
        userService.expireStr("test","测试数据");
    }
    @Test
    void test1(){
        User user = userService.selectById("10086");

        System.out.println(user);
    }


}
