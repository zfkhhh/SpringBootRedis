package com.lettuce;

import com.lettuce.po.Article;
import com.lettuce.service.ListCacheServiceImpl;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

/**
 * Author : zfk
 * Data : 9:32
 */
@SpringBootTest
public class ListTests {
    @Autowired
    private ListCacheServiceImpl listCacheService;

    @Test
    public void test1(){
        listCacheService.initArticle();
    }
    @Test
    public void select(){
        List<Article> articles = listCacheService.selectArticleTop5();

        articles.stream().forEach(System.out::println);
    }
}
