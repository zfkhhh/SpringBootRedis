package com.lettuce.po;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * Author : zfk
 * Data : 9:15
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Article implements Serializable {

    private String id;
    private String title;
    private String  author;
    private String createData;
    private Integer clickNum;
}
