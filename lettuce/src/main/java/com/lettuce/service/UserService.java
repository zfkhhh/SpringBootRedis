package com.lettuce.service;

import com.lettuce.po.User;

/**
 * Author : zfk
 * Data : 17:01
 */
public interface UserService {
    public String getString(String key);
    public void expireStr(String key,String value);
    public User selectById(String id);
}
