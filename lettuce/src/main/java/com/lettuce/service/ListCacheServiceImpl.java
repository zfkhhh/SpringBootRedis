package com.lettuce.service;

import com.lettuce.po.Article;
import lombok.extern.java.Log;
import lombok.extern.log4j.Log4j;
import org.springframework.data.redis.core.ListOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * Author : zfk
 * Data : 9:17
 */
@Service
@Log
public class ListCacheServiceImpl {

    private RedisTemplate<String,Object> redisTemplate;
    private static String key = "article:top5";
    @Resource(name = "redisTemplate")
    ListOperations<String, Article> opsForList;

    //显示当前最近的5条文章信息
    public void initArticle(){

        log.info("模拟从Mysql查询");
        ArrayList<Article> list = new ArrayList<>();
        for (int i = 1;i < 6;i++){
            Article article = new Article(String.valueOf(i),"title"+i,"author"+i,"Data"+i,i*100);
            list.add(article);
        }
        opsForList.rightPushAll(key,list);

    }

    //显示最新的5条文章信息
    public List<Article> selectArticleTop5(){
        log.info("查询Redis，显示最近5条文章信息");
        List<Article> articleList = opsForList.range(key, 0, 4);
        return articleList;
    }

}
