package com.lettuce.service;

import com.lettuce.po.User;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.stereotype.Service;


import javax.annotation.Resource;
import java.util.concurrent.TimeUnit;

/**
 * Author : zfk
 * Data : 16:56
 */
@Service
@Slf4j
public class UserServiceImpl implements UserService {

    @Autowired
    private RedisTemplate<String, Object> redisTemplate;

    @Resource(name = "redisTemplate")
    private ValueOperations<String, Object> valueOps;

    /**
     * Redis命令 ==》 Lettuce => RedisTemplate进一步的封装
     * redis String类型
     * 用户输入一个key,先判断Redis是否存在数据，存在就在Redis中查询，
     * 不存在就在Mysql数据库查询(模拟)。将结果返回给Redis
     */
    @Override
    public String getString(String key) {

        log.info("RedisTemplate ==> 测试");

        String value = null;
        //hasKey 相当于 exist
        if (redisTemplate.hasKey(key)) {
            log.info("=== Redis查询到数据 ===");
            return (String) valueOps.get(key);
        } else {
            //模拟查询Mysql数据库 = > 先查询数据库
            value = "RedisTemplate => lettuce";
            log.info("Redis没有查询到，存入:" + value);
            //再删除缓存
            redisTemplate.delete(key);
        }
        return value;

    }

    /**
     * 用户输入一个redis数据，设置有效期为1小时
     *
     * @param key
     * @param value
     */
    @Override
    public void expireStr(String key, String value) {
        //存值
        redisTemplate.opsForValue().set(key, value);
        redisTemplate.expire(key, 1, TimeUnit.HOURS);
    }

    /**
     * 根据Id查询用户对象信息
     *
     * @param id
     * @return 如果存在返回，不存在从Mysql查找(模拟)插入Redis
     */
    @Override
    public User selectById(String id) {

        //key => user:id
        //redisTemplate对Hash数据key进行了封装
        //redisTemplate.opsForHash().hasKey("user",id) = exists user:id
        if (redisTemplate.opsForHash().hasKey("user", id)) {
            log.info("-----》查询Redis");
            return (User) redisTemplate.opsForHash().get("user", id);
        } else {
            log.info("----》查询Mysql数据库");
            User user = new User(id, "张三", 23);
            //put(h,hk,hv) => h:用户实体user hk:用户主键id hv:整个对象
            redisTemplate.opsForHash().put("user", id, user);
            return user;
        }

    }





}
