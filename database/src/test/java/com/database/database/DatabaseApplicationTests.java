package com.database.database;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;

@SpringBootTest
class DatabaseApplicationTests {


    @Autowired
    private RedisTemplate firstRedisTemplate;

    @Autowired
    private RedisTemplate secondRedisTemplate;

    @Autowired
    private RedisTemplate redisTemplate2;

    @Test
    void contextLoads() {

        ValueOperations operations = firstRedisTemplate.opsForValue();
        operations.set("age", "25");
        String age = (String) operations.get("age");
        System.out.println("年龄："+age);

        ValueOperations operations2 = secondRedisTemplate.opsForValue();
        operations2.set("address", "beijing");

        String address = (String) operations2.get("address");
        System.out.println("地址："+address);
    }


    @Test
    public void test(){
        //操作本地Redis连接
        ValueOperations valueOperations = redisTemplate2.opsForValue();
        valueOperations.set("age","26");
        System.out.println("本地Redis=> age:"+valueOperations.get("age").toString());

        //操作远程Redis连接
        ValueOperations operations = firstRedisTemplate.opsForValue();
        String age = (String) operations.get("age");
        System.out.println("远程Redis=> age:"+age);

    }
}
