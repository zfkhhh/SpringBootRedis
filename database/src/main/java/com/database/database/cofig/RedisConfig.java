package com.database.database.cofig;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.lettuce.LettuceConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.StringRedisTemplate;

/**
 * Author : zfk
 * Data : 8:57
 */

@Configuration
@EnableAutoConfiguration
public class RedisConfig {


    @Value("${redis.database.first}")
    private int firstDatabase;

    @Value("${redis.database.second}")
    private int secondDatabase;

    @Value("${redis.host}")
    private String host;

    @Value("${redis.password}")
    private String password;

    @Value("${redis.port}")
    private int port;

    @Value("${redis.timeout}")
    private int timeout;


    @Bean(name = "firstRedisTemplate")
    public RedisTemplate getFirstRedisTemplate() {

        LettuceConnectionFactory connectionFactory = new LettuceConnectionFactory();

        connectionFactory.setDatabase(firstDatabase);
        connectionFactory.setHostName(host);
        connectionFactory.setPassword(password);
        connectionFactory.setPort(port);
        connectionFactory.setTimeout(timeout);
        connectionFactory.afterPropertiesSet();            //记得添加这行！

        StringRedisTemplate stringRedisTemplate = new StringRedisTemplate(connectionFactory);
        return stringRedisTemplate;
    }


    @Bean(name = "secondRedisTemplate")
    public RedisTemplate getSecondRedisTemplate() {

        LettuceConnectionFactory connectionFactory = new LettuceConnectionFactory();
        connectionFactory.setDatabase(secondDatabase);
        connectionFactory.setHostName(host);
        connectionFactory.setPassword(password);
        connectionFactory.setPort(port);
        connectionFactory.setTimeout(timeout);
        connectionFactory.afterPropertiesSet();

        StringRedisTemplate stringRedisTemplate = new StringRedisTemplate(connectionFactory);
        return stringRedisTemplate;
    }


}

