package com.database.database.cofig;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.lettuce.LettuceConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.StringRedisTemplate;

/**
 * Author : zfk
 * Data : 10:46
 */
@Configuration
@EnableAutoConfiguration
public class RedisConfig2 {


    @Value("${redis2.database}")
    private int database = 0;

    @Value("${redis2.host}")
    private String host;

    @Value("${redis2.password}")
    private String password;

    @Value("${redis2.port}")
    private int port;

    @Value("${redis2.timeout}")
    private int timeout;

    @Bean(name = "redisTemplate2")
    RedisTemplate getRedisTemplate2(){
        
        LettuceConnectionFactory factory = new LettuceConnectionFactory();
        factory.setDatabase(database);
        factory.setPort(port);
        factory.setHostName(host);
        factory.setPassword(password);
        factory.setTimeout(timeout);
        factory.afterPropertiesSet();

        StringRedisTemplate stringRedisTemplate = new StringRedisTemplate(factory);
        return stringRedisTemplate;
    }

}
