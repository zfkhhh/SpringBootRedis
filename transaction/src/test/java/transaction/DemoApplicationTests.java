package transaction;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.dao.DataAccessException;
import org.springframework.data.redis.core.RedisOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.SessionCallback;
import org.springframework.data.redis.core.ValueOperations;

import javax.annotation.Resource;
import java.util.List;

@SpringBootTest
class DemoApplicationTests {


    @Autowired
    private RedisTemplate<String,Object> redisTemplate;

    @Resource(name = "redisTemplate")
    private ValueOperations<String, Object> valueOps;


    @Test
    public void contextLoads() {


        valueOps.set("key1","value1");

        List list = (List) redisTemplate.execute(new SessionCallback() {

            @Override
            public Object execute(RedisOperations operations) throws DataAccessException {
                // 设置要监控key1
                operations.watch("key1");
                // 开启事务，在exec命令执行前，进入队列
                operations.multi();
                operations.opsForValue().set("key2", "value2");
                operations.opsForValue().set("key1", "update_value1");

                // 获取值将为null，因为redis只是把命令放入队列，
                Object value2 = operations.opsForValue().get("key2");
                System.out.println("命令在队列，所以value为null【" + value2 + "】");

                //operations.opsForValue().increment("key1");
                operations.opsForValue().set("key3", "value3");
                Object value3 = operations.opsForValue().get("key3");
                System.out.println("命令在队列，所以value为null【" + value3 + "】");

                // 执行exec命令，将先判别key1是否在监控后被修改过，如果是不执行事务，否则执行事务
                return operations.exec();
            }

        });

        list.stream().forEach(System.out::println);
    }

}
