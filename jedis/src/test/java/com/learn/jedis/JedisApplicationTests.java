package com.learn.jedis;

import com.learn.jedis.po.User;
import com.learn.jedis.service.UserService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;

@SpringBootTest
class JedisApplicationTests {
    @Autowired
    private UserService userService;


/*    @Autowired
    private JedisPool jedisPool;
    @Test
    void contextLoads() {
        System.out.println(jedisPool);
    }*/

    @Test
    void test1(){
        String result = userService.getString("name");
        System.out.println("Value = "+result);
    }

    @Test
    void test2(){
        String key = "testKey";
        String value = "测试数据";
        userService.expireStr(key,value);

    }

    @Test
    void test3(){
        User user = userService.selectById("1111");
        System.out.println(user.toString());
    }

    @Test
    void test4(){
        Jedis jedis = new Jedis("localhost",6379);
        String name = jedis.get("name");

        System.out.println(name);

        jedis.close();
    }

    @Test
    void test5(){

    }
}
