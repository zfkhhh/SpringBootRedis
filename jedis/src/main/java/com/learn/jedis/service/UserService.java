package com.learn.jedis.service;

import com.learn.jedis.po.User;

/**
 * Author : zfk
 * Data : 19:37
 */
public interface UserService {

    /**
     * redis String类型
     * 用户输入一个key,先判断Redis是否存在数据，存在就在Redis中查询，
     * 不存在就在Mysql数据库查询。将结果返回给Redis
     */
    public String getString(String key);


    public void expireStr(String key,String value);

    public User selectById(String id);
}
