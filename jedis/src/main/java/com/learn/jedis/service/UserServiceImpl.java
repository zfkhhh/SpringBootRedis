package com.learn.jedis.service;

import com.learn.jedis.po.User;
import lombok.extern.java.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;

import java.util.HashMap;
import java.util.Map;


/**
 * Author : zfk
 * Data : 19:40
 */
@Service
@Log
public class UserServiceImpl implements UserService {



    //Redis - Jedis连接池
    @Autowired
    private JedisPool jedisPool;

    /**
     * Redis命令 对应 Jedis方法
     * redis String类型
     * 用户输入一个key,先判断Redis是否存在数据，存在就在Redis中查询，
     * 不存在就在Mysql数据库查询(模拟)。将结果返回给Redis
     */
    @Override
    public String getString(String key) {

        String value = null;
        //Jedis对象，连接信息
        Jedis jedis = jedisPool.getResource();
        //jedis.exists(key) <==> Redis : exists key
        if (jedis.exists(key)){
            //key存在
            log.info("查询Redis中的数据");
            return jedis.get(key);
        }
        else {
            value = "Mysql";
            log.info("Redis数据不存在，查询Mysql数据库"+value);
            jedis.set(key,value);
        }
        //关闭连接
        jedis.close();

        return value;

    }


    /**
     * 测试有效期
     * 需求：用户输入redis数据，该key的有效期为1小时
     */
    @Override
    public void expireStr(String key, String value){

        Jedis jedis = jedisPool.getResource();
        jedis.set(key,value);
        jedis.expire(key,1 * 60 * 60);
        log.info(key + "\t 设置值："+value+"\t 1h");
        jedis.close();
    }


    /**
     * Redis - Hash类型
     * 存一个对象
     * 用户在前端传入一个ID编号，查询用户对象
     * 先到Redis查询，如果Redis存在直接返回结果
     * 不存在就查询Mysql(模拟)，返回查询结果，赋值给Redis
     */
    @Override
    public User selectById(String id){
        //key => 表名：id
        String key ="user:"+id;
        //得到Jedis对象
        Jedis jedis = jedisPool.getResource();

        User user = null;
        //判断
        if (jedis.exists(key)){
            //存在，打印输出
            log.info("=== 查询Redis数据库 ===");
            Map<String, String> map = jedis.hgetAll(key);
            user = new User(map.get("id"), map.get("name"), Integer.parseInt(map.get("age")));
        }
        else {
            log.info("=== 查询Mysql数据库 ===");
            user = new User(id,"张三",19);
            HashMap<String, String> map = new HashMap<>();
            map.put("id",user.getId());
            map.put("name",user.getName());
            map.put("age",user.getAge().toString());
            jedis.hmset(key,map);
            log.info("=== 存入Redis中 ===");

        }
        return user;
    }
}
